import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
    render() {
        return (
            <div>
                <div>
                    <button className={'button-add'} onClick={this.props.clickButton}>Add</button>
                </div>
                <ul>
                    {this.props.list.map((item, index) => (
                        <li key={index}>
                            {item}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default TodoList;

