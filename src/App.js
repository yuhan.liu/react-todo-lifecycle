import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

const refreshPage = () => {
    location.reload()
};

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true,
            list: [],
        };
        this.showTodoList = this.showTodoList.bind(this);
        this.hideTodoList = this.hideTodoList.bind(this);
        this.addTodo = this.addTodo.bind(this);
    }

    showTodoList() {
        this.setState({
            show: false
        })
    }

    hideTodoList() {
        this.setState({
            show: true,
            list: []
        })
    }

    clickRefresh() {
        refreshPage();
    }

    addTodo() {
        const newItem = `List Title ${this.state.list.length + 1}`;
        this.setState({
            list: [...this.state.list, newItem]
        });
    }

    render() {
        return (
            <div className='App'>
                <div className={'button'}>
                    {this.state.show ? <button onClick={this.showTodoList}>Show</button> :
                        <button onClick={this.hideTodoList}>Hide</button>}
                    <button className={'refresh'} onClick={this.clickRefresh}>Refresh</button>
                </div>
                <div>
                    {this.state.show ? '' : <TodoList list={this.state.list} clickButton={this.addTodo}/>}
                </div>
            </div>
        )
    }
}

export default App;